FROM node:16-buster-slim

RUN npm install

EXPOSE 3000

RUN npm run start
